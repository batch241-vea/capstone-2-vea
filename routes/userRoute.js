const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");


//Route for User Registration
router.post('/register', (req, res) => {

	userController.registerUser(req.body).then((resultFromController) => {
		res.send(resultFromController);
	});
});



//Route for User Login
router.post('/login', (req, res) => {

	userController.loginUser(req.body).then(resultFromController => {
		res.send(resultFromController);
	});
});



//Route for creating order
router.post('/createOrder', auth.verify, (req, res) => {
	
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity,
		totalAmount: req.body.totalAmount		
	}

	if(data.isAdmin){
		res.send(`An admin can't order!`);

	} else {
		userController.createOrder(req.body).then(resultFromController => {
			res.send(resultFromController);
		})
	}
})



//Route for retrieving user
/*router.get('/:userId', (req, res) => {
	const userData = auth.decode(req.headers.authorization);


	userController.retrieveUser(req.params).then(resultFromController => {
		res.send(resultFromController)
	})
})*/




//[STRETCH GOAL]
//Route to set user as admin
router.patch('/:userId', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		userController.setAsAdmin(req.params, req.body).then(resultFromController => {
		res.send(resultFromController)
		})

	} else {
		res.send(`Non-admin user can't perform this action`);
	}
})

//[ADDED] (For Frontend)
// Route to Retrieve user details 
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		//console.log(userData)
		//console.log(req.headers.authorization);

	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


// Route for checking if the user's email already exists in our database
router.post('/checkEmail', (req, res) => {

	userController.checkEmailExists(req.body).then((resultFromController) => {
		res.send(resultFromController);
	}) 
})


router.get('/allProducts', (req, res) => {

	userController.findAllActive().then(resultFromController => {
		res.send(resultFromController);
	})
});



module.exports = router;