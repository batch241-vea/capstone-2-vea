const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');

const auth = require("../auth");


//Route for creating product
router.post('/createProduct', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.createProduct(req.body).then(resultFromController => {
			res.send(resultFromController);
		})

	} else {
		res.send(`Failed to create product!`);
	}
});



//Route for retrieving all active products
router.get('/allActive', (req, res) => {

	productController.findAllActive().then(resultFromController => {
		res.send(resultFromController);
	})
});



//Route for retrieving specific product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => {
		res.send(resultFromController)
	})
})



//Route for updating product information
router.put('/:productId', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController => {
			res.send(resultFromController)
		})

	} else {
		res.send(`Failed to update product!`);
	}
})



//Route for archiving product
router.patch('/:productId', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.archiveProduct(req.params, req.body).then(resultFromController => {
			res.send(resultFromController);
		})

	} else {
		res.send(`Failed to archive product!`);
	}
})


//[ADDED] (For Frontend)
//Route for unarchiving product
router.patch('/activate/:productId', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.archiveProduct(req.params, req.body).then(resultFromController => {
			res.send(resultFromController);
		})

	} else {
		res.send(`Failed to archive product!`);
	}
})








module.exports = router;