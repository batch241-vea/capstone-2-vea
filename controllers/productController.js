const Product = require("../models/Product");
const auth = require('../auth');


//Add product
module.exports.createProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((course, error) => {
		
		if (error){
			return false
		} else {
			return true
		}
	})
}



//Retrieve active products
module.exports.findAllActive = () => {

	return Product.find({isActive: true}).then(result => {
		return result;
	})
}



//Retrieve specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}



//Update a product
module.exports.updateProduct = (reqParams, reqBody) => {

	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((course, error) => {
		if (error){
			return false
		} else {
			return true
		}
	})
}



//Archive a product
module.exports.archiveProduct = (reqParams, reqBody) => {

	let archiveProduct = {
		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((course, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}


//[ADDED] (For Frontend)
module.exports.archiveProduct = (reqParams, reqBody) => {

	let archiveProduct = {
		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((course, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}

