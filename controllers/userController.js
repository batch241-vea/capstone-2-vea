const User = require('../models/User');
const Product = require("../models/Product");
const bcrypt = require('bcrypt');
const auth = require('../auth');


//Register user but check first if the email exists
module.exports.registerUser = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

//		if(result.length > 0) {
//			return `Failed to register! This email has already been used!`

//		} else {
			let newUser = new User({
				
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				isAdmin: reqBody.isAdmin,
			})

			return newUser.save().then((user, error) => {

				if(error){
					return false
					
				} else {
					return true
				}
			})

//		}
	}) 
}



//Log in user
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if (result == null) {
			return false

		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}

			} else {
				return false
			}

		}
	})
}



//Create an order
module.exports.createOrder = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.orders.push({products: [{
			productName: data.productName,
			quantity: data.quantity	}],
		totalAmount: data.totalAmount
	});

		return user.save().then((user, error) => {

			if (error) {
				return false
			} else {
				return true
			}

		})
	})


	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.orders.push({orderId: data.userId});

		return product.save().then((product, error) => {
			if (error) {
				return false
			} else {
				return true
			}

		})
	})


	if(isUserUpdated && isProductUpdated){
		return true
	} else {
		return false
	}

}



//Retrieve user
/*module.exports.retrieveUser = (reqParams) => {

	return User.findById(reqParams.userId).then(result => {
		return result;
	})
}*/



//[STRETCH GOAL]
//Set user as admin
module.exports.setAsAdmin = (reqParams, reqBody) => {

	let setAsAdmin = {
		isAdmin: reqBody.isAdmin
	}

	return User.findByIdAndUpdate(reqParams.userId, setAsAdmin).then((user, error) => {
		if(error){
			return false
		} else {
			return `The user has been set as Admin`
		}
	})
}

//[ADDED] (For Frontend)
// Retrieve user details (For Frontend)
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};


//Checking if the email exists in the database
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email:reqBody.email}).then(result => {

		if(result.length > 0) {
			return true
		//No duplicate email found, not yet registered
		} else {
			return false
		}
	})
}


//Retrieve all products
module.exports.findAllActive = () => {

	return Product.find({}).then(result => {
		return result;
	})
}