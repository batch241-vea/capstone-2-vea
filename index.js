// Dependency Setup
const express = require('express');

const mongoose = require('mongoose');

const cors = require('cors');

const userRoute = require("./routes/userRoute");

const productRoute = require("./routes/productRoute");


// Server Setup
const port = 4000;

const app = express();

app.use(cors());

app.use(express.json());

app.use(express.urlencoded({extended: true}));


// Database Connection
mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.wnkvmfq.mongodb.net/Capstone-2-VEA?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, "Connection Eror"));
db.once('open', () => console.log("We're connected to the cloud database"));



app.use('/users', userRoute);
app.use('/products', productRoute);


app.listen(port, () => console.log(`Now listening to port: ${port}`));